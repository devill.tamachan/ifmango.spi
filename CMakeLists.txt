cmake_minimum_required(VERSION 3.5)
project(ifmango)

set(CMAKE_CXX_STANDARD 14)

SET(BUILD_SHARED_LIBS OFF)
include_directories(../mango/include/)
#link_directories(../mango-master/build/build32/Release/)

set(CMAKE_CXX_FLAGS_RELEASE "/MT")

add_library(ifmango SHARED ifmango.cpp ifmango.def)
#target_link_libraries(ifmango LINK_PUBLIC mango)
set_target_properties(ifmango PROPERTIES PREFIX "")
#set_target_properties(ifmango PROPERTIES OUTPUT_NAME "ifmango")
set_target_properties(ifmango PROPERTIES SUFFIX ".spi")