[![Build status](https://ci.appveyor.com/api/projects/status/fdhrk1ga8one3a4w?svg=true)](https://ci.appveyor.com/project/tamachan/ifmango-spi)

JPEGのSUSIEプラグイン。MangaMeeya7.3が使う機能だけ実装。使わないAPIは未実装

[mangoライブラリ https://github.com/t0rakka/mango](https://github.com/t0rakka/mango) 使用