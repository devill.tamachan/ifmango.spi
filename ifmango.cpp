
#define USE_MMAP

#include <mango/mango.hpp>
#include <windows.h>

using namespace mango;
using namespace mango::filesystem;

//
// spi00in.lzh
// ■サンプルソース
// 自由に改変して使用して下さい。
// '00IN'BMPプラグインのソース
// http://www.asahi-net.or.jp/~kh4s-smz/spi/make_spi.html
//

#pragma pack(push)
#pragma pack(1) //構造体のメンバ境界を1バイトにする
typedef struct PictureInfo {
	long left,top;		/* 画像を展開する位置 */
	long width;			/* 画像の幅(pixel) */
	long height;		/* 画像の高さ(pixel) */
	WORD x_density;		/* 画素の水平方向密度 */
	WORD y_density;		/* 画素の垂直方向密度 */
	short colorDepth;	/* 画素当たりのbit数 */
	HLOCAL hInfo;		/* 画像内のテキスト情報 */
} PictureInfo;
#pragma pack(pop)


#define SPI_NO_FUNCTION			-1	/* その機能はインプリメントされていない */
#define SPI_ALL_RIGHT			0	/* 正常終了 */
#define SPI_ABORT				1	/* コールバック関数が非0を返したので展開を中止した */
#define SPI_NOT_SUPPORT			2	/* 未知のフォーマット */
#define SPI_OUT_OF_ORDER		3	/* データが壊れている */
#define SPI_NO_MEMORY			4	/* メモリーが確保出来ない */
#define SPI_MEMORY_ERROR		5	/* メモリーエラー */
#define SPI_FILE_READ_ERROR		6	/* ファイルリードエラー */
#define	SPI_WINDOW_ERROR		7	/* 窓が開けない (非公開のエラーコード) */
#define SPI_OTHER_ERROR			8	/* 内部エラー */
#define	SPI_FILE_WRITE_ERROR	9	/* 書き込みエラー (非公開のエラーコード) */
#define	SPI_END_OF_FILE			10	/* ファイル終端 (非公開のエラーコード) */


typedef int (CALLBACK *SPI_PROGRESS)(int, int, long);


static const char *pluginfo[] = {
  "00IN",       /* Plug-in API バージョン */
  "ifmango",    /* Plug-in名,バージョン及び copyright */
  "*.jpg;*.jpeg;*.jpe",      /* 代表的な拡張子 ("*.JPG" "*.JPG;*.JPEG" など) */
  "JPEG file(*.jpg)", /* ファイル形式名 */
};

/*int WCHARtoUTF8(wchar_t *src, char **dst)
{
  if(src==NULL || dst==NULL)return 0;
  
  int lenU8Path = WideCharToMultiByte(CP_UTF8, 0, src, -1, NULL, 0, NULL, NULL);
  if(lenU8Path==0)
  {
    return 0;
  }
  char *bufU8Path = new char[lenU8Path+1];
  lenU8Path = WideCharToMultiByte(CP_UTF8, 0, src, -1, bufU8Path, lenU8Path, NULL, NULL);
  if(lenU8Path==0)
  {
    delete bufU8Path;
    return 0;
  }
  *dst = bufU8Path;
  return lenU8Path;
}
*/
int MBtoWCHAR(char *src, wchar_t **dst)
{
  if(src==NULL || dst==NULL)return 0;
  
  int lenWPath = MultiByteToWideChar(CP_ACP, 0, src, -1, NULL, 0);
  if(lenWPath==0)
  {
    return 0;
  }
  wchar_t *bufWPath = new wchar_t[lenWPath+1];
  lenWPath = MultiByteToWideChar(CP_ACP, 0, src, -1, bufWPath, lenWPath);
  if(lenWPath==0)
  {
    delete bufWPath;
    return 0;
  }
  *dst = bufWPath;
  return lenWPath;
}

extern "C" int __stdcall GetPluginInfo(int infono, LPSTR buf, int buflen)
{
  if (infono < 0 || infono >= (sizeof(pluginfo) / sizeof(char *))) 
    return 0;

  lstrcpyn(buf, pluginfo[infono], buflen);

  return lstrlen(buf);
}

extern "C" int __stdcall IsSupported(LPSTR filename, DWORD dw)
{
  unsigned char *data;
  unsigned char buff[2];
  DWORD ReadBytes;

  if ((dw & 0xFFFF0000) == 0) {
  /* dwはファイルハンドル */
    if (!ReadFile((HANDLE)dw, buff, 2, &ReadBytes, NULL)) {
      return 0;
    }
    data = buff;
  } else {
  /* dwはバッファへのポインタ */
    data = (unsigned char *)dw;
  }

  if(data[0]==0xFF && data[1]==0xD8)
  {
    return 1;
  }
  return 0;
}

extern "C" int __stdcall GetPictureInfo
(LPSTR buf, long len, unsigned int flag, struct PictureInfo *lpInfo)
{
  return SPI_NO_FUNCTION;
  /*
  int ret = SPI_OTHER_ERROR;
  char headbuf[HEADBUF_SIZE];
  char *data;
  char *filename;
  HANDLE hf;
  DWORD ReadBytes;

  if ((flag & 7) == 0) {
  // bufはファイル名
    hf = CreateFile(buf, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
    if (hf == INVALID_HANDLE_VALUE) return SPI_FILE_READ_ERROR;
    if (SetFilePointer(hf, len, NULL, FILE_BEGIN) != (DWORD)len) {
      CloseHandle(hf);
      return SPI_FILE_READ_ERROR;
    }
    if (!ReadFile(hf, headbuf, HEADBUF_SIZE, &ReadBytes, NULL)) {
      CloseHandle(hf);
      return SPI_FILE_READ_ERROR;
    }
    CloseHandle(hf);
    if (ReadBytes != HEADBUF_SIZE) return SPI_NOT_SUPPORT;
    data = headbuf;
    filename = buf;
  } else {
  // bufはファイルイメージへのポインタ
    if (len < HEADBUF_SIZE) return SPI_NOT_SUPPORT;
    data = (char *)buf;
    filename = NULL;
  }

  if (!IsSupportedEx(filename, data)) {
    ret = SPI_NOT_SUPPORT;
  } else {
    ret = GetPictureInfoEx(data, lpInfo);
  }

  return ret;*/
}

/*Surface*/void myload_surface(Memory memory, const std::string& extension, HANDLE *pHBInfo, HANDLE *pHBm)
{
  Surface surface(0, 0, Format(), 0, nullptr);

  ImageDecoder decoder(memory, extension);
  if (decoder.isDecoder())
  {
    ImageHeader header = decoder.header();

    // configure surface
    surface.width  = header.width;
    surface.height = header.height;
    //surface.format = header.format;
    surface.format = Format(32, Format::UNORM, Format::BGRA, 8, 8, 8, 8);
    surface.stride = (surface.width * surface.format.bytes() + 3) & ~3;
    *pHBInfo = LocalAlloc(LMEM_FIXED|LMEM_ZEROINIT, sizeof(BITMAPINFOHEADER));
    if(*pHBInfo == NULL)return ;//surface;
    BITMAPINFOHEADER *pBmpInfo = (BITMAPINFOHEADER*)(*pHBInfo);
    pBmpInfo->biSize = sizeof(BITMAPINFOHEADER);
    pBmpInfo->biWidth = surface.width;
    pBmpInfo->biHeight= -surface.height;
    pBmpInfo->biPlanes= 1;
    pBmpInfo->biBitCount = surface.format.bits;
    *pHBm    = LocalAlloc(LMEM_FIXED, surface.height * surface.stride);
    if(*pHBm == NULL)
    {
      LocalFree(*pHBInfo);
      *pHBInfo = NULL;
      return ;//surface;
    }
    surface.image  = (u8*)(*pHBm);

    // decode
    ImageDecodeStatus status = decoder.decode(surface);
    MANGO_UNREFERENCED(status);
  }

  return ;//surface;
}
    
extern "C" int __stdcall GetPicture(
    LPSTR buf, long len, unsigned int flag, 
    HANDLE *pHBInfo, HANDLE *pHBm,
    SPI_PROGRESS lpPrgressCallback, long lData)
{

  if ((flag & 7) == 0)
  {
    // OutputDebugStringA("GetPicture: filename");
    return SPI_NO_FUNCTION;
    
    /* bufはファイル名 */
    /*wchar_t *bufW = NULL;
    int lenW = MBtoWCHAR(buf, &bufW);
    if(lenW<=0)return SPI_OTHER_ERROR;
    File file(u16_toBytes(bufW));
    delete(bufW);
    Bitmap bitmap(file, ".jpg");*/
  } else {
    // OutputDebugStringA("GetPicture: mem");
    /* bufはファイルイメージへのポインタ */
    /*Surface s(*/myload_surface(Memory((u8*)buf, len), ".jpg", pHBInfo, pHBm);//);
    if(*pHBInfo == NULL || *pHBm == NULL)return SPI_OTHER_ERROR;
    return SPI_ALL_RIGHT;
  }
}

extern "C" int __stdcall GetPreview(
  LPSTR buf, long len, unsigned int flag,
  HANDLE *pHBInfo, HANDLE *pHBm,
  SPI_PROGRESS lpPrgressCallback, long lData)
{
  //return GetPicture(buf, len, flag, pHBInfo, pHBm, lpPrgressCallback, lData);
   // OutputDebugStringA("GetPreview");
  return SPI_NO_FUNCTION;
}

BOOL APIENTRY DllMain(HANDLE hModule, DWORD ul_reason_for_call, LPVOID lpReserved)
{
   // OutputDebugStringA("ifmango");
   switch(ul_reason_for_call)
   {
    case DLL_PROCESS_ATTACH:
    
      /*{
        HMODULE hModule = NULL;
        GetModuleHandleExA(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS|GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT, (LPCSTR)DllMain, &hModule);
        DisableThreadLibraryCalls(hModule);
      }*/
      break;
    case DLL_PROCESS_DETACH:
    
      //ThreadPool &pool = ThreadPool::getInstance();
      //pool.snuff();
      
      break;
  }
  return TRUE;
}